select 	PICKS.CATEGORIE1,
	PICKS.CATEGORIE2,
	PICKS.CATEGORIE3,
	PICKS.EVENT,
	PICKS.EVENTNAME,
	PICKS.BET,
	PICKS.ODDSTYPE,
	TO_TIMESTAMP(PICKS.TIME::NUMERIC / 1000) - CURRENT_TIMESTAMP AS TIME_LEFT,
	coalesce(pickshistorystatisticsoddstype.sumpicks,0) as history_picks,
	coalesce(pickshistorystatisticsoddstype.realprofit,0) as history_profit,
	coalesce(statisticsaarzb.sumpicks,0) as aarzba_picks,
	coalesce(statisticsaarzb.realprofit,0) as aarzba_profit,
	coalesce(activepickshistorystatisticsoddstype.sumpicks,0) as active_picks,
	coalesce(activepickshistorystatisticsoddstype.realprofit,0) as active_profit,
	event_pick(picks.event) = picks.id as bet_this,
	pick_order.order_pick,
	pick_order.stake,
	pick_order.action_pick,
	pick_order.min_odds_history,
	pick_order.max_odds_history
from picks

LEFT JOIN pickshistorystatisticsoddstype
ON pickshistorystatisticsoddstype.ID = PICKS.statisticsoddstypeid
LEFT JOIN pickshistorystatisticsoddstype statisticsaarzb
ON statisticsaarzb.ID = '542eab14e4b063d9e20ca850'||PICKS.CATEGORIE1||PICKS.PRICE||PICKS.ODDSTYPE
LEFT JOIN activepickshistorystatisticsoddstype
ON activepickshistorystatisticsoddstype.ID = PICKS.statisticsoddstypeid
left join pick_order
on	((coalesce(pickshistorystatisticsoddstype.sumpicks,0) > pick_order.min_picks and pick_order.min_picks<> -1) or (coalesce(pickshistorystatisticsoddstype.sumpicks,0) = 0 and pick_order.min_picks = -1))
and	(coalesce(pickshistorystatisticsoddstype.realprofit,0) >= pick_order.min_odds_history and coalesce(pickshistorystatisticsoddstype.realprofit,0) < pick_order.max_odds_history)
and     ((coalesce(statisticsaarzb.sumpicks,0) > pick_order.min_picks_aarzb and pick_order.min_picks_aarzb <> -1) or (coalesce(statisticsaarzb.sumpicks,0) = 0 and pick_order.min_picks_aarzb = -1))
and	(coalesce(statisticsaarzb.realprofit,0) >= pick_order.min_aarzb and coalesce(statisticsaarzb.realprofit,0) < pick_order.max_aarzb)
and	(coalesce(activepickshistorystatisticsoddstype.realprofit,0) >= pick_order.min_odds_active and coalesce(activepickshistorystatisticsoddstype.realprofit,0) < pick_order.max_odds_active)
WHERE TO_TIMESTAMP(PICKS.TIME::NUMERIC / 1000) - CURRENT_TIMESTAMP > '00:00:00'
ORDER BY TO_TIMESTAMP(PICKS.TIME::NUMERIC / 1000),  eventname	