curl 'https://api.pyckio.com/twips' 
-H 'Authorization: Bearer 8e9326d0-77b2-4327-9660-34036615db9a' 
-H 'Origin: https://pyckio.com' 
-H 'Accept-Encoding: gzip, deflate' 
-H 'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4' 
-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36' 
-H 'Content-Type: application/json' 
-H 'Accept: application/json, text/javascript, */*; q=0.01' 
-H 'Referer: https://pyckio.com/i/' 
-H 'X-Requested-With: XMLHttpRequest' 
-H 'Connection: keep-alive' 
--data-binary '{"event":"571f3ce4e4b0c4d0ba2146f2",
				"pick":{"categories":["soccer","soccer-brazil","soccer-brazil-cup"],
				"oddsType":"1",
				"price":7.99,
				"bet":"AWAY",
				"stake":1}}' 
--compressed


response 
{
  "id" : "571fbfb6e4b0cf0b3c380637",
  "user" : "55e4f6c0e4b08a8ee2835316",
  "userStatic" : {
    "id" : "55e4f6c0e4b08a8ee2835316",
    "username" : "addax",
    "category" : "soccer",
    "type" : "TRAINEE",
    "avatar" : "https://secure.gravatar.com/avatar/7f18cde7c1096f0a0da054d6e5160461",
    "picks" : 13,
    "yield" : 0.1207,
    "averageOdds" : 1.72,
    "rating" : 0.0
  },
  "involved" : [ "55e4f6c0e4b08a8ee2835316" ],
  "likes" : [ ],
  "replies" : [ ],
  "message" : null,
  "event" : "571f3ce4e4b0c4d0ba2146f2",
  "msgLength" : 0,
  "eventName" : "Ceara - Resende RJ",
  "eventSlug" : "soccer-brazil-cup-ceara-resende-rj-201604262215",
  "created" : "2016-04-26T19:21:26.966Z",
  "updated" : "2016-04-26T19:21:26.966Z",
  "deleted" : null,
  "language" : "en",
  "pick" : {
    "categories" : [ "soccer", "soccer-brazil", "soccer-brazil-cup" ],
    "betName" : "Resende RJ",
    "oddsType" : "1",
    "bet" : "AWAY",
    "line" : 0.0,
    "price" : 7.99,
    "stake" : 1
  },
  "consumed" : false,
  "pro" : false,
  "free" : false,
  "active" : true,
  "spam" : null
}