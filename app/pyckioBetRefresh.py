#!/usr/bin/python
import urllib2
import httplib
import json
import subprocess
import time
import datetime
import psycopg2
from StringIO import StringIO
import gzip

validToken = False
headers = {            
        'Accept': 'application/json, text/javascript, */*; q=0.01',                        
        'Accept-Language':'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',        
        'Connection':'keep-alive',
        'Host':'api.pyckio.com',
        'Origin':'https://pyckio.com',
        'Referer':'https://pyckio.com/i/',
        'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36',
        'X-Requested-With':'XMLHttpRequest'                        
    }		

try:
    #CONN = psycopg2.connect("dbname='pyckio' user='postgres' host='localhost' password='postgres'")
    CONN = psycopg2.connect("dbname='postgrespyckio' user='postgrespyckio' host='postgrespyckio.cztvazmbaunt.us-west-2.rds.amazonaws.com' password='postgrespyckio'")
    
except:
    print "I am unable to connect to the database"

##funcao que chega o token e preenche o header caso o token for valido
def checkToken():

    ##Seleciona o utlimo token inserido que nao foi deletado
    cur = CONN.cursor()
    cur.execute("""select pyckiotoken.token from pyckiotoken where deleted is null order by created desc LIMIT 1""")
    rows = cur.fetchall()

    validToken = False

    if(rows.__len__() > 0):
        print 'Token Encontrado : %s' % rows[0][0]      
        headers['Authorization'] = rows[0][0]   
        
        urlRequest = 'https://api.pyckio.com/users?filter=username:addax'
        request = urllib2.Request(urlRequest , headers=headers)         

        try:
            response = urllib2.urlopen(request)             
            validToken = True
        except urllib2.HTTPError, e:
            print 'Token Invalido'
            del headers['Authorization']
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])           
        except urllib2.URLError, e:
            print 'Token Invalido' 
            del headers['Authorization']              
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
        except httplib.HTTPException, e:
            print 'Token Invalido'
            del headers['Authorization']
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
        except Exception:   
            print 'Token Invalido'
            del headers['Authorization']
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
    else:
        print 'Token nao encontrado'

    cur.close()         
    CONN.commit()

    print 'Status Token : %s' % validToken  

def refreshDimBet():
	print "Inicio de atualizacao da tabelas pick prev"    
	sql1 = '''
        delete from pick_prev
    '''
	sql2 = '''
    	with pick_prev as (
		select 	PICKS.id,
	PICKS.user,
	PICKS.twip,
	PICKS.EVENT,
	PICKS.EVENTNAME,
	PICKS.eventslug,
	picks.time,
	PICKS.CATEGORIE1,
	PICKS.CATEGORIE2,
	PICKS.CATEGORIE3,
	picks.beatname,
	picks.oddstype,		
	PICKS.BET,	
	picks.price,
	picks.stake,
	picks.line,
	picks.profit,
	picks.active,
	picks.created,
	picks.updated,
	picks.deleted,
	picks.pro,
	picks.free,
	picks.loadedat,
	picks.createdid,
	picks.startat,
	picks.deletedid,
	picks.statisticsid,
	picks.statisticsoddstypeid,
	picks.startatid,
	picks.statisticsoddstypelineid,
	picks.statisticsidall,
	picks.statisticsoddstypeidall,
	(date_part('epoch',(TO_TIMESTAMP(PICKS.TIME::NUMERIC / 1000) - CURRENT_TIMESTAMP)) / 60)::numeric(18,2) AS TIME_LEFT,
	coalesce(pickshistorystatisticsoddstype.sumpicks,0) as history_picks,
	coalesce(pickshistorystatisticsoddstype.realprofit,0) as history_profit,
	coalesce(statisticsaarzb.sumpicks,0) as aarzba_picks,
	coalesce(statisticsaarzb.realprofit,0) as aarzba_profit,
	coalesce(activepickshistorystatisticsoddstype.sumpicks,0) as active_picks,
	coalesce(activepickshistorystatisticsoddstype.realprofit,0) as active_profit,
	(coalesce(event_pick(picks.event) = picks.id,false))::text as bet_this,
	coalesce(pick_order.order_pick,0) as order_pick,
	coalesce(pick_order.stake,0)as stake_pick,
	(coalesce(pick_order.action_pick,false))::text as action_pick
from picks

LEFT JOIN pickshistorystatisticsoddstype
ON pickshistorystatisticsoddstype.ID = PICKS.statisticsoddstypeid
LEFT JOIN pickshistorystatisticsoddstype statisticsaarzb
ON statisticsaarzb.ID = '542eab14e4b063d9e20ca850'||PICKS.CATEGORIE1||PICKS.PRICE||PICKS.ODDSTYPE
LEFT JOIN activepickshistorystatisticsoddstype
ON activepickshistorystatisticsoddstype.ID = PICKS.statisticsoddstypeid
left join pick_order
on	((coalesce(pickshistorystatisticsoddstype.sumpicks,0) > pick_order.min_picks and pick_order.min_picks<> -1) or (coalesce(pickshistorystatisticsoddstype.sumpicks,0) = 0 and pick_order.min_picks = -1))
and	(coalesce(pickshistorystatisticsoddstype.realprofit,0) >= pick_order.min_odds_history and coalesce(pickshistorystatisticsoddstype.realprofit,0) < pick_order.max_odds_history)
and     ((coalesce(statisticsaarzb.sumpicks,0) > pick_order.min_picks_aarzb and pick_order.min_picks_aarzb <> -1) or (coalesce(statisticsaarzb.sumpicks,0) = 0 and pick_order.min_picks_aarzb = -1))
and	(coalesce(statisticsaarzb.realprofit,0) >= pick_order.min_aarzb and coalesce(statisticsaarzb.realprofit,0) < pick_order.max_aarzb)
and	(coalesce(activepickshistorystatisticsoddstype.realprofit,0) >= pick_order.min_odds_active and coalesce(activepickshistorystatisticsoddstype.realprofit,0) < pick_order.max_odds_active)
WHERE TO_TIMESTAMP(PICKS.TIME::NUMERIC / 1000) - CURRENT_TIMESTAMP > '00:00:00'
ORDER BY TO_TIMESTAMP(PICKS.TIME::NUMERIC / 1000),  eventname )
		INSERT INTO pick_prev SELECT *  FROM pick_prev;    
    '''    
	cur = CONN.cursor()
	cur.execute(sql1)
	cur.execute(sql2)   
	cur.close()
	CONN.commit()


def main():
    CONTA = 'addax'

    checkToken()
    refreshDimBet()    
    CONN.close()


if __name__ == "__main__":
	main()
