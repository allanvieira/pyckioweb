#!/usr/bin/python
import urllib2
import httplib
import json
import subprocess
import time
import datetime
import psycopg2
from StringIO import StringIO
import gzip

PICKSHISTORY = 0;
headers = {            
        'Accept': 'application/json, text/javascript, */*; q=0.01',                                
        'Accept-Language':'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',        
        'Connection':'keep-alive',
        'Host':'api.pyckio.com',
        'Origin':'https://pyckio.com',
        'Referer':'https://pyckio.com/i/',
        'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36',
        'X-Requested-With':'XMLHttpRequest'                        
    }

try:
    #CONN = psycopg2.connect("dbname='pyckio' user='postgres' host='localhost' password='postgres'")
    CONN = psycopg2.connect("dbname='postgrespyckio' user='postgrespyckio' host='postgrespyckio.cztvazmbaunt.us-west-2.rds.amazonaws.com' password='postgrespyckio'")
    
except:
    print "I am unable to connect to the database"

##funcao que chega o token e preenche o header caso o token for valido
def checkToken():

    ##Seleciona o utlimo token inserido que nao foi deletado
    cur = CONN.cursor()
    cur.execute("""select pyckiotoken.token from pyckiotoken where deleted is null order by created desc LIMIT 1""")
    rows = cur.fetchall()

    validToken = False

    if(rows.__len__() > 0):
        print 'Token Encontrado : %s' % rows[0][0]      
        headers['Authorization'] = rows[0][0]   
        
        urlRequest = 'https://api.pyckio.com/users?filter=username:addax'
        request = urllib2.Request(urlRequest , headers=headers)         

        try:
            response = urllib2.urlopen(request)             
            validToken = True
        except urllib2.HTTPError, e:
            print 'Token Invalido'
            del headers['Authorization']
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])           
        except urllib2.URLError, e:
            print 'Token Invalido' 
            del headers['Authorization']              
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
        except httplib.HTTPException, e:
            print 'Token Invalido'
            del headers['Authorization']
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
        except Exception:   
            print 'Token Invalido'
            del headers['Authorization']
            cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
    else:
        print 'Token nao encontrado'

    cur.close()         
    CONN.commit()

    print 'Status Token : %s' % validToken    

##funcao que carrega o nome dos novos usuarios
def getUsersDetails():
    headers['Accept-Encoding'] = 'gzip, deflate, sdch'
    print 'Iniciando Carga de detalhes dos usuarios'
    ROWINSERT = 0
  
    request = urllib2.Request("https://api.pyckio.com/users/55e4f6c0e4b08a8ee2835316/followees?_=1458761312093", headers=headers)
    APIFollowees= urllib2.urlopen(request)
    buf = StringIO( APIFollowees.read())
    f = gzip.GzipFile(fileobj=buf)
    data = f.read()
    followees = json.loads(data)

    for follow in followees:
        cur = CONN.cursor()
        cur.execute("""SELECT id from dim_user where id='%s'""" % (follow['id']) )

        rows = cur.fetchall()
        if(rows.__len__() == 0):

            cur2 = CONN.cursor()
            cur2.execute("""INSERT INTO dim_user(id, name, type) VALUES ('%s', '%s', '%s') """ % ( follow['id'],follow['username'].replace('\'',''),follow['type'].replace('\'','') ))
            ROWINSERT+=1
            cur2.close()
    CONN.commit()

    print 'Novos Usuarios Seguidos : ' + ROWINSERT.__str__()
##funcao que insere registros na picksactivehistory e remove as aposta inativas da picks
def refreshAtivePicks():
    print "Inicio de atualizacao da tabelas picksactivehistory"    
    sql1 = '''
        WITH upd AS (
                        select  picks.id,
            picks.user,
            picks.twip,
            picks.event,
            picks.eventname,
            picks.eventslug,
            picks.time,
            picks.categorie1,
            picks.categorie2,
            picks.categorie3,
            picks.beatname,
            picks.oddstype,
            picks.bet,
            picks.price,
            picks.stake,
            picks.line,     
            picks.active,
            picks.created,
            picks.updated,
            picks.deleted,
            picks.pro,
            picks.free,
            picks.loadedat,
            picks.deletedid,
            picks.createdid,
            picks.startat,
            pickshistory.result_1,
            pickshistory.profit,
            pickshistory.realprofit,
            pickshistory.won,
            picks.statisticsid,
            pickshistory.half_lost,
            pickshistory.half_won,            
            pickshistory.lost,
            pickshistory.void,
            pickshistory.zero,
            picks.statisticsoddstypeid,
            picks.startatid,
            picks.statisticsoddstypelineid,
            picks.statisticsidall,
            picks.statisticsoddstypeidall
            from picks
            inner join pickshistory
            on  pickshistory.id = picks.id
            where (select picksactivehistory.id from picksactivehistory where picksactivehistory.id = picks.id) is null)
            INSERT INTO picksactivehistory SELECT *  FROM upd;    
    '''
    sql2 = '''
        delete from picks where (select pickshistory.id from pickshistory where pickshistory.id = picks.id) is not null
    '''
    cur = CONN.cursor()
    cur.execute(sql1)
    cur.execute(sql2)
    cur.close()
    CONN.commit()

##Funcao que faz o calculo de perc ganho, sum profit por usuario, categori1 e price
def refreshCalcs():
    print "Inicio de atualizacao da tabela pickshistorystatistics"
    sql1 = """delete from pickshistorystatistics where id <> '0'"""
    sql2 = '''
    WITH upd AS (
        SELECT  pickshistory.user,
                pickshistory.categorie1,
                pickshistory.price,
                (sum(pickshistory.won)::numeric(18,2) / count(pickshistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(pickshistory.id) as sumpicks,
                sum(pickshistory.won) as won,
                sum(pickshistory.profit) as sumprofit,
                (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id))::numeric(18,2) as realprofit,
                sum(pickshistory.lost) as lost,
                sum(pickshistory.half_lost) as half_lost,
                sum(pickshistory.half_won) as half_won,
                sum(pickshistory.zero) as zero,
                sum(pickshistory.void) as void,
                case
                    when count(pickshistory.id) < 20 then 'INVALID'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) <= -8 then 'CONTRA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                pickshistory.user||pickshistory.categorie1||pickshistory.price as statisticsid
        from pickshistory   
        group by pickshistory.user,
        pickshistory.categorie1,
        pickshistory.price
    )
    INSERT INTO pickshistorystatistics SELECT *  FROM upd;
    '''
    sql3 = """delete from pickshistorystatisticsoddstype where id <> '0'"""
    sql4 = '''
    WITH upd AS (
        SELECT  pickshistory.user,
                pickshistory.categorie1,
                pickshistory.price,
                pickshistory.oddstype,
                (sum(pickshistory.won)::numeric(18,2) / count(pickshistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(pickshistory.id) as sumpicks,
                sum(pickshistory.won) as won,
                sum(pickshistory.profit) as sumprofit,
                (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id))::numeric(18,2) as realprofit,
                sum(pickshistory.lost) as lost,
                sum(pickshistory.half_lost) as half_lost,
                sum(pickshistory.half_won) as half_won,
                sum(pickshistory.zero) as zero,
                sum(pickshistory.void) as void,
                case
                    when count(pickshistory.id) < 20 then 'INVALID'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) <= -8 then 'CONTRA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                pickshistory.user||pickshistory.categorie1||pickshistory.price||pickshistory.oddstype as statisticsid
        from pickshistory   
        group by pickshistory.user,
        pickshistory.categorie1,
        pickshistory.price,
        pickshistory.oddstype
    )
    INSERT INTO pickshistorystatisticsoddstype SELECT *  FROM upd;
    '''
    sql5 = """delete from pickshistorystatisticsoddstypeline where id <> '0'"""
    sql6 = '''
    WITH upd AS (
        SELECT  pickshistory.user,
                pickshistory.categorie1,
                pickshistory.price,
                pickshistory.oddstype,
                pickshistory.line,
                (sum(pickshistory.won)::numeric(18,2) / count(pickshistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(pickshistory.id) as sumpicks,
                sum(pickshistory.won) as won,
                sum(pickshistory.profit) as sumprofit,
                (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id))::numeric(18,2) as realprofit,
                sum(pickshistory.lost) as lost,
                sum(pickshistory.half_lost) as half_lost,
                sum(pickshistory.half_won) as half_won,
                sum(pickshistory.zero) as zero,
                sum(pickshistory.void) as void,
                case
                    when count(pickshistory.id) < 20 then 'INVALID'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) <= -8 then 'CONTRA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                pickshistory.user||pickshistory.categorie1||pickshistory.price||pickshistory.oddstype||pickshistory.line as statisticsid
        from pickshistory   
        group by pickshistory.user,
        pickshistory.categorie1,
        pickshistory.price,
        pickshistory.oddstype,
        pickshistory.line
    )
    INSERT INTO pickshistorystatisticsoddstypeline SELECT *  FROM upd
    '''
    sql7 = """delete from allpickshistorystatistics where id <> '0'"""
    sql8 = '''
    WITH upd AS (
        SELECT  pickshistory.user,
        dim_user.name as username,
                pickshistory.categorie1,
                pickshistory.price,
                (sum(pickshistory.won)::numeric(18,2) / count(pickshistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(pickshistory.id) as sumpicks,
                sum(pickshistory.won) as won,
                sum(pickshistory.profit) as sumprofit,
                (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id))::numeric(18,2) as realprofit,
                sum(pickshistory.lost) as lost,
                sum(pickshistory.half_lost) as half_lost,
                sum(pickshistory.half_won) as half_won,
                sum(pickshistory.zero) as zero,
                sum(pickshistory.void) as void,
                case
                    when count(pickshistory.id) < 20 then 'INVALID'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) <= -8 then 'CONTRA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                pickshistory.categorie1||pickshistory.price as statisticsid
        from pickshistory   
        left join dim_user
        on dim_user.id = pickshistory.user
        group by pickshistory.user,
        pickshistory.categorie1,
        pickshistory.price,
    dim_user.name        
    )
    INSERT INTO allpickshistorystatistics SELECT *  FROM upd;
    '''
    sql9 = """delete from allpickshistorystatisticsoddstype where id <> '0'"""
    sql10 = '''
    WITH upd AS (
        SELECT  pickshistory.user,
        dim_user.name as username,
                pickshistory.categorie1,
                pickshistory.price,
                pickshistory.oddstype,
                (sum(pickshistory.won)::numeric(18,2) / count(pickshistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(pickshistory.id) as sumpicks,
                sum(pickshistory.won) as won,
                sum(pickshistory.profit) as sumprofit,
                (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id))::numeric(18,2) as realprofit,
                sum(pickshistory.lost) as lost,
                sum(pickshistory.half_lost) as half_lost,
                sum(pickshistory.half_won) as half_won,
                sum(pickshistory.zero) as zero,
                sum(pickshistory.void) as void,
                case
                    when count(pickshistory.id) < 20 then 'INVALID'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) <= -8 then 'CONTRA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((pickshistory.price * sum(pickshistory.won)) + (pickshistory.price  * sum(pickshistory.half_won) / 2) + (sum(pickshistory.half_lost) /2) + sum(pickshistory.void) + sum(pickshistory.zero)) - count(pickshistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                pickshistory.categorie1||pickshistory.price||pickshistory.oddstype as statisticsid
        from pickshistory   
        left join dim_user
        on dim_user.id = pickshistory.user
        group by pickshistory.user,
        pickshistory.categorie1,
        pickshistory.price,
    dim_user.name,
    pickshistory.oddstype
    )
    INSERT INTO allpickshistorystatisticsoddstype SELECT *  FROM upd;
    '''
    sql11 = """delete from activepickshistorystatistics where id <> '0'"""
    sql12 = '''
    WITH upd AS (
        SELECT  picksactivehistory.user,
                picksactivehistory.categorie1,
                picksactivehistory.price,
                (sum(picksactivehistory.won)::numeric(18,2) / count(picksactivehistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(picksactivehistory.id) as sumpicks,
                sum(picksactivehistory.won) as won,
                sum(picksactivehistory.profit) as sumprofit,
                (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id))::numeric(18,2) as realprofit,
                sum(picksactivehistory.lost) as lost,
                sum(picksactivehistory.half_lost) as half_lost,
                sum(picksactivehistory.half_won) as half_won,
                sum(picksactivehistory.zero) as zero,
                sum(picksactivehistory.void) as void,
                case
                    when count(picksactivehistory.id) < 20 then 'INVALID'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) <= -8 then 'CONTRA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                picksactivehistory.user||picksactivehistory.categorie1||picksactivehistory.price as statisticsid
        from picksactivehistory   
        group by picksactivehistory.user,
        picksactivehistory.categorie1,
        picksactivehistory.price
    )
    INSERT INTO activepickshistorystatistics SELECT *  FROM upd;
    '''
    sql13 = """delete from activepickshistorystatisticsoddstype where id <> '0'"""
    sql14 = '''
    WITH upd AS (
        SELECT  picksactivehistory.user,
                picksactivehistory.categorie1,
                picksactivehistory.price,
                picksactivehistory.oddstype,
                (sum(picksactivehistory.won)::numeric(18,2) / count(picksactivehistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(picksactivehistory.id) as sumpicks,
                sum(picksactivehistory.won) as won,
                sum(picksactivehistory.profit) as sumprofit,
                (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id))::numeric(18,2) as realprofit,
                sum(picksactivehistory.lost) as lost,
                sum(picksactivehistory.half_lost) as half_lost,
                sum(picksactivehistory.half_won) as half_won,
                sum(picksactivehistory.zero) as zero,
                sum(picksactivehistory.void) as void,
                case
                    when count(picksactivehistory.id) < 20 then 'INVALID'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) <= -8 then 'CONTRA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                picksactivehistory.user||picksactivehistory.categorie1||picksactivehistory.price||picksactivehistory.oddstype as statisticsid
        from picksactivehistory   
        group by picksactivehistory.user,
        picksactivehistory.categorie1,
        picksactivehistory.price,
        picksactivehistory.oddstype
    )
    INSERT INTO activepickshistorystatisticsoddstype SELECT *  FROM upd;
    '''
    sql15 = """delete from activepickshistorystatisticsoddstypeline where id <> '0'"""
    sql16 = '''
    WITH upd AS (
        SELECT  picksactivehistory.user,
                picksactivehistory.categorie1,
                picksactivehistory.price,
                picksactivehistory.oddstype,
                picksactivehistory.line,
                (sum(picksactivehistory.won)::numeric(18,2) / count(picksactivehistory.id)::numeric(18,2))::numeric(18,2) as percwon,
                count(picksactivehistory.id) as sumpicks,
                sum(picksactivehistory.won) as won,
                sum(picksactivehistory.profit) as sumprofit,
                (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id))::numeric(18,2) as realprofit,
                sum(picksactivehistory.lost) as lost,
                sum(picksactivehistory.half_lost) as half_lost,
                sum(picksactivehistory.half_won) as half_won,
                sum(picksactivehistory.zero) as zero,
                sum(picksactivehistory.void) as void,
                case
                    when count(picksactivehistory.id) < 20 then 'INVALID'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) <= -8 then 'CONTRA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between -8 and 0 then 'MONITORAR'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between 0 and 2.5 then 'MEDIA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) between 2.5 and 12 then 'ALTA'
                    when (((picksactivehistory.price * sum(picksactivehistory.won)) + (picksactivehistory.price  * sum(picksactivehistory.half_won) / 2) + (sum(picksactivehistory.half_lost) /2) + sum(picksactivehistory.void) + sum(picksactivehistory.zero)) - count(picksactivehistory.id)) > 12 then 'HOT'
                    else 'INDEFINIDO'
                end as PRECISION,
                picksactivehistory.user||picksactivehistory.categorie1||picksactivehistory.price||picksactivehistory.oddstype||picksactivehistory.line as statisticsid
        from picksactivehistory   
        group by picksactivehistory.user,
        picksactivehistory.categorie1,
        picksactivehistory.price,
        picksactivehistory.oddstype,
        picksactivehistory.line
    )
    INSERT INTO activepickshistorystatisticsoddstypeline SELECT *  FROM upd
    '''
    cur = CONN.cursor()
    cur.execute(sql1)
    cur.execute(sql2)
    cur.execute(sql3)
    cur.execute(sql4)
    cur.execute(sql5)
    cur.execute(sql6)
    cur.execute(sql7)
    cur.execute(sql8)
    cur.execute(sql9)
    cur.execute(sql10)
    cur.execute(sql11)
    cur.execute(sql12)
    cur.execute(sql13)
    cur.execute(sql14)
    cur.execute(sql15)
    cur.execute(sql16)
    cur.close()
    CONN.commit()

#Funcao responsalvel pro realizar a carega de apostas ativas
def getPickActive(idAccount):
    print 'Carga de Apostas Ativas'
    OFFSET = 0
    LOADMORE = True
    ROWCOUNT = 0
    ROWINSERT = 0

    while LOADMORE:
        ##carrega pagina apostas
        urlRequest = 'https://api.pyckio.com/users/'+idAccount +'/picks?active=true&page='+OFFSET.__str__()
        request = urllib2.Request(urlRequest , headers=headers)         
        APIPicks = urllib2.urlopen(request)            
        data = json.load(APIPicks)

        for dt in data:
            ROWCOUNT+=1
            ##verificar se o id ja esta no banco
            cur = CONN.cursor()
            cur.execute("""SELECT id from picks where id='%s'""" % (dt['id']) )
            rows = cur.fetchall()
            if(rows.__len__() == 0):
                deleted = ''
                categorie1 = dt['categories'][0]
                categorie2 = 'Null'
                categorie3 = 'Null'
                if dt['deleted'] is None :
                    deleted = 'Null'
                else:
                    deleted = dt['deleted']
                if dt['categories'].__len__() == 3:
                    categorie2 = dt['categories'][1]
                    categorie3 = dt['categories'][2]
                elif dt['categories'].__len__() == 2:
                    categorie2 = dt['categories'][1]

                datee = datetime.datetime.strptime(dt['created'].split('.')[0].replace('T',' '),'%Y-%m-%d %H:%M:%S')
                strcreatedid = datee.year.__str__()
                if datee.month < 10:
                    strcreatedid = strcreatedid + datee.month.__str__()
                else:
                    strcreatedid = strcreatedid + '0' + datee.month.__str__()
                if datee.day < 10:
                    strcreatedid = strcreatedid + datee.day.__str__()
                else:
                    strcreatedid = strcreatedid + '0' + datee.day.__str__()  
                timestamp = datetime.datetime.fromtimestamp(dt['time']/1000)                                      
                strtimestamp = timestamp.strftime('%Y-%m-%d %H:%M:%S')
                strstartedid = timestamp.year.__str__()
                if timestamp.month < 10:
                    strstartedid = strstartedid + timestamp.month.__str__()
                else:
                    strstartedid = strstartedid + '0' + timestamp.month.__str__()
                if timestamp.day < 10:
                    strstartedid = strstartedid + timestamp.day.__str__()
                else:
                    strstartedid = strstartedid + '0' + timestamp.day.__str__()  
                ##Calculo fk das estatisticas    
                statisticsid = dt['user'].replace('\'','') + categorie1.replace('\'','') + dt['price'].__str__()
                cur.execute("""SELECT id from pickshistorystatistics where id='%s'""" % (statisticsid))
                rows = cur.fetchall()
                if(rows.__len__() == 0):
                    statisticsid = '0'                    
                statisticsoddstypeid = dt['user'].replace('\'','') + categorie1.replace('\'','') + dt['price'].__str__() + dt['oddsType'].replace('\'','')
                cur.execute("""SELECT id from pickshistorystatisticsoddstype where id='%s'""" % (statisticsoddstypeid))
                rows = cur.fetchall()
                if(rows.__len__() == 0):
                    statisticsoddstypeid = '0'                    
                statisticsoddstypelineid = dt['user'].replace('\'','') + categorie1.replace('\'','') + dt['price'].__str__() + dt['oddsType'].replace('\'','') + dt['line'].__str__()
                cur.execute("""SELECT id from pickshistorystatisticsoddstypeline where id='%s'""" % (statisticsoddstypelineid))
                rows = cur.fetchall()
                if(rows.__len__() == 0):
                    statisticsoddstypelineid = '0'
                statisticsidall = categorie1.replace('\'','') + dt['price'].__str__()   
                cur.execute("""SELECT id from allpickshistorystatistics where id='%s'""" % (statisticsidall))
                rows = cur.fetchall()
                if(rows.__len__() == 0):
                    statisticsidall = '0'
                statisticsoddstypeidall = categorie1.replace('\'','') + dt['price'].__str__() + dt['oddsType'].replace('\'','')
                cur.execute("""SELECT id from allpickshistorystatisticsoddstype where id='%s'""" % (statisticsoddstypeidall))
                rows = cur.fetchall()
                if(rows.__len__() == 0):
                    statisticsoddstypeidall = '0'

                cur2 = CONN.cursor()
                cur2.execute("""
                    INSERT INTO picks(  id, "user", twip, event, eventname, eventslug, 
                                        "time", categorie1,categorie2, categorie3, beatname, oddstype, 
                                        bet, price, stake, line, profit, active, 
                                        created, updated, deleted, pro, free, loadedat, 
                                        createdid, startat,statisticsid,statisticsoddstypeid,startatid, statisticsoddstypelineid,
                                        statisticsidall, statisticsoddstypeidall)
                    VALUES (    '%s', '%s', '%s', '%s', '%s', '%s', 
                                '%s', '%s', '%s', '%s', '%s', '%s', 
                                '%s', %s  , '%s',   %s,   %s, '%s', 
                                '%s', '%s',   %s, '%s', '%s', current_timestamp, 
                                %s  ,'%s','%s', '%s', %s, '%s',
                                '%s', '%s') """
                        % ( dt['id'],
                        dt['user'].replace('\'',''),
                        dt['twip'].replace('\'',''),
                        dt['event'].replace('\'',''),
                        dt['eventName'].replace('\'',''),
                        dt['eventSlug'].replace('\'',''),
                        dt['time'],
                        categorie1.replace('\'',''),
                        categorie2.replace('\'',''),
                        categorie3.replace('\'',''),
                        dt['betName'].replace('\'',''),
                        dt['oddsType'].replace('\'',''),
                        dt['bet'],
                        dt['price'],
                        dt['stake'],
                        dt['line'],
                        dt['profit'],
                        dt['active'],                        
                        dt['created'],
                        dt['updated'],
                        deleted,
                        dt['pro'],
                        dt['free'],
                        strcreatedid,
                        strtimestamp,
                        statisticsid,
                        statisticsoddstypeid,
                        strstartedid,
                        statisticsoddstypelineid,
                        statisticsidall,
                        statisticsoddstypeidall))
                ROWINSERT+=1
                cur2.close()        
        OFFSET+=1
        if  data.__len__() == 0:
            LOADMORE = False
    CONN.commit()        

    print 'Final da Carga de Apostas Ativas'

    print 'Paginas Carregadas : ' + (OFFSET + 1).__str__()
    print 'Linhas Lidas : ' + ROWCOUNT.__str__()
    print 'Linhas Inseridas : ' + ROWINSERT.__str__()
    #print 'Apostas Inativadas : ' + rows.__len__().__str__()


#Funcao responsavel por realizar a carga de historico de apostas de uma conta
def getPickHistory(idAccount):
    print 'Carga de Historico de Apostas'
    OFFSET = 0
    LOADMORE = True
    ROWCOUNT = 0
    ROWINSERT = 0

    while LOADMORE:
        ##carrega pagina com historico
        urlRequest = 'https://api.pyckio.com/users/'+idAccount +'/picks?active=false&page='+OFFSET.__str__()
        request = urllib2.Request(urlRequest , headers=headers)         
        APIPickHistory = urllib2.urlopen(request)
        data = json.load(APIPickHistory)

        for dt in data:
            ROWCOUNT+=1
            ##verificar se o id ja esta no banco
            cur = CONN.cursor()
            cur.execute("""SELECT id from pickshistory where id='%s'""" % (dt['id']) )
            rows = cur.fetchall()
            if(rows.__len__() == 0):
                deleted = ''
                categorie1 = dt['categories'][0]
                categorie2 = 'Null'
                categorie3 = 'Null'
                won = 0
                lost = 0
                half_won = 0
                half_lost = 0
                void = 0
                zero = 0
                datee = datetime.datetime.strptime(dt['created'].split('.')[0].replace('T',' '),'%Y-%m-%d %H:%M:%S')
                strcreatedid = datee.year.__str__()
                if datee.month < 10:
                    strcreatedid = strcreatedid + datee.month.__str__()
                else:
                    strcreatedid = strcreatedid + '0' + datee.month.__str__()
                if datee.day < 10:
                    strcreatedid = strcreatedid + datee.day.__str__()
                else:
                    strcreatedid = strcreatedid + '0' + datee.day.__str__()
                if dt['result'] == 'WON' :
                    won = 1
                elif dt['result'] == 'LOST' :
                    lost = 1
                elif dt['result'] == 'HALF_WON' :
                    half_won = 1                    
                elif dt['result'] == 'HALF_LOST' :
                    half_lost = 1                                        
                elif dt['result'] == 'VOID' :
                    void = 1                    
                elif dt['result'] == 'ZERO' :
                    zero = 1                    
                if dt['deleted'] is None :
                    deleted = 'Null'
                else:
                    deleted = dt['deleted']
                if dt['categories'].__len__() == 3:
                    categorie2 = dt['categories'][1]
                    categorie3 = dt['categories'][2]
                elif dt['categories'].__len__() == 2:
                    categorie2 = dt['categories'][1]
                realprofit = dt['price'] * won + (dt['price']  * (half_won) / 2) + ((half_lost) /2) + (void) + (zero) - 1

                cur2 = CONN.cursor()
                teste = ''
                cur2.execute("""
                    INSERT INTO pickshistory(id, "user", twip, event, eventname, eventslug, "time", categorie1,categorie2, categorie3, beatname, oddstype, bet, price, stake,line, profit, active, result_1, created, updated, deleted, pro,free, loadedat, won, half_lost, half_won, lost, void, zero,createdid,realprofit)
                    VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', %s, %s, '%s', '%s', '%s', %s, '%s', '%s', current_timestamp, %s, %s, %s, %s, %s, %s, %s, %s) """
                        % ( dt['id'],
                        dt['user'].replace('\'',''),
                        dt['twip'].replace('\'',''),
                        dt['event'].replace('\'',''),
                        dt['eventName'].replace('\'',''),
                        dt['eventSlug'].replace('\'',''),
                        dt['time'],
                        categorie1.replace('\'',''),
                        categorie2.replace('\'',''),
                        categorie3.replace('\'',''),
                        dt['betName'].replace('\'',''),
                        dt['oddsType'].replace('\'',''),
                        dt['bet'],
                        dt['price'],
                        dt['stake'],
                        dt['line'],
                        dt['profit'],
                        dt['active'],
                        dt['result'],
                        dt['created'],
                        dt['updated'],
                        deleted,
                        dt['pro'],
                        dt['free'],
                        won,
                        half_lost,
                        half_won,
                        lost,
                        void,
                        zero,
                        strcreatedid,
                        realprofit))
                ROWINSERT+=1
                cur2.close()
        if  ROWINSERT == ROWCOUNT:
            OFFSET+=1
        else:
            LOADMORE = False        
        if  data.__len__() == 0:
            LOADMORE = False
        CONN.commit()

    print 'Final da Carga de Historico de Apostas'

    print 'Paginas Carregadas : ' + (OFFSET + 1).__str__()
    print 'Linhas Lidas : ' + ROWCOUNT.__str__()
    print 'Linhas Inseridas : ' + ROWINSERT.__str__()
    PICKSHISTORY = ROWINSERT

    cur.close()


def main():
    CONTA = 'addax'

    checkToken()

    urlRequest = 'https://api.pyckio.com/users?filter=username:'+CONTA
    request = urllib2.Request(urlRequest , headers=headers)
    response = urllib2.urlopen(request) 
    #gzipJson = response.read()
    #buf = StringIO(gzipJson)
    #f = gzip.GzipFile(fileobj=buf)    
    data = json.load(response)        

    followees = data['followees']    
    for follow in followees:
        #Verifica se usuario ja esta na base, se nao estiver faz a carga
        cur = CONN.cursor()
        cur.execute("""select id from pickshistory where pickshistory.user = '%s' LIMIT 1""" % follow )
        rows = cur.fetchall()
        cur.close()
        if(rows.__len__() == 0):
            print 'Conta : ' + follow
            print 'Inicio ' + (time.strftime("%H:%M:%S"))
            getPickHistory(follow)
            getPickActive(follow)
            print 'Fim ' +  (time.strftime("%H:%M:%S"))    
    refreshCalcs()
    refreshAtivePicks()
    if 'Authorization' in headers:
        getUsersDetails()
    CONN.close()



if __name__ == "__main__":
	main()
