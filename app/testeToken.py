#!/usr/bin/python
import urllib2
import json
import subprocess
import time
import datetime
import psycopg2
from StringIO import StringIO
import gzip

try:
    CONN = psycopg2.connect("dbname='pyckio' user='postgres' host='localhost' password='postgres'")
except:
    print "I am unable to connect to the database"

validToken = False
headers = {            
        'Accept': 'application/json, text/javascript, */*; q=0.01',                        
        'Accept-Language':'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',        
        'Connection':'keep-alive',
        'Host':'api.pyckio.com',
        'Origin':'https://pyckio.com',
        'Referer':'https://pyckio.com/i/',
        'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36',
        'X-Requested-With':'XMLHttpRequest'                        
    }

cur = CONN.cursor()
cur.execute("""select pyckiotoken.token from pyckiotoken where deleted is null order by created desc LIMIT 1""")
rows = cur.fetchall()


if(rows.__len__() > 0):
	print 'Token Encontrado : %s' % rows[0][0]		
	headers['Authorization'] = rows[0][0]	
	
	urlRequest = 'https://api.pyckio.com/users?filter=username:addax'
	request = urllib2.Request(urlRequest , headers=headers)        	

	try:
		response = urllib2.urlopen(request)	
		json = json.load(response)		
		validToken = True
	except urllib2.HTTPError, e:
		print 'Token Invalido'
		del headers['Authorization']
		cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])		    
	except urllib2.URLError, e:
		print 'Token Invalido' 
		del headers['Authorization']              
		cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
	except httplib.HTTPException, e:
		print 'Token Invalido'
		del headers['Authorization']
		cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
	except Exception:	
		print 'Token Invalido'
		del headers['Authorization']
		cur.execute("""UPDATE PYCKIOTOKEN SET DELETED = CURRENT_TIMESTAMP WHERE TOKEN = '%s'""" % rows[0][0])
else:
	print 'Token nao encontrado'

cur.close()        	
CONN.commit()

print 'Status Token : %s' % validToken		
print headers