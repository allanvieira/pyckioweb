--limpar tabela
--delete from pick_order
--inserir dados
with pick_order as(
SELECT 1 AS order_pick, 20 AS min_picks, 2.5 as min_odds_history, 12 as max_odds_history, 20 as min_picks_aarzb, 0 as min_aarzb, 2.5 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 5 as stake, true as action_pick union all
SELECT 2 AS order_pick, 20 AS min_picks, 2.5 as min_odds_history, 12 as max_odds_history, 0 as min_picks_aarzb, -10000 as min_aarzb, 10000 as max_aarzb, -10000 as min_odds_active, 10000 as max_odds_active, 5 as stake, true as action_pick union all
SELECT 3 AS order_pick, 20 AS min_picks, 0 as min_odds_history, 2.5 as max_odds_history, 20 as min_picks_aarzb, 2.5 as min_aarzb, 12 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 5 as stake, true as action_pick union all
SELECT 4 AS order_pick, 20 AS min_picks, 12 as min_odds_history, 10000 as max_odds_history, -1 as min_picks_aarzb, -10000 as min_aarzb, 10000 as max_aarzb, -10000 as min_odds_active, 10000 as max_odds_active, 3 as stake, true as action_pick union all
SELECT 5 AS order_pick, 20 AS min_picks, 2.5 as min_odds_history, 12 as max_odds_history, 20 as min_picks_aarzb, 2.5 as min_aarzb, 12 as max_aarzb, -10000 as min_odds_active, 10000 as max_odds_active, 3 as stake, true as action_pick union all
SELECT 6 AS order_pick, 20 AS min_picks, 0 as min_odds_history, 2.5 as max_odds_history, 0 as min_picks_aarzb, 0 as min_aarzb, -10000 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 3 as stake, true as action_pick union all
SELECT 7 AS order_pick, 20 AS min_picks, -10000 as min_odds_history, -8 as max_odds_history, 20 as min_picks_aarzb, -10000 as min_aarzb, -8 as max_aarzb, -10000 as min_odds_active, 10000 as max_odds_active, 3 as stake, false as action_pick union all
SELECT 8 AS order_pick, 20 AS min_picks, -10000 as min_odds_history, -8 as max_odds_history, 20 as min_picks_aarzb, -8 as min_aarzb, 0 as max_aarzb, -10000 as min_odds_active, 0 as max_odds_active, 3 as stake, false as action_pick union all
SELECT 9 AS order_pick, 20 AS min_picks, -10000 as min_odds_history, -8 as max_odds_history, -1 as min_picks_aarzb, -10000 as min_aarzb, 0 as max_aarzb, -10000 as min_odds_active, 10000 as max_odds_active, 3 as stake, false as action_pick union all
SELECT 10 AS order_pick, 20 AS min_picks, -8 as min_odds_history, 0 as max_odds_history, 20 as min_picks_aarzb, -8 as min_aarzb, 0 as max_aarzb, -10000 as min_odds_active, 0 as max_odds_active, 3 as stake, false as action_pick union all
SELECT 11 AS order_pick, 20 AS min_picks, -8 as min_odds_history, 0 as max_odds_history, -1 as min_picks_aarzb, -10000 as min_aarzb, 0 as max_aarzb, -10000 as min_odds_active, 0 as max_odds_active, 3 as stake, false as action_pick union all
SELECT 12 AS order_pick, 20 AS min_picks, -8 as min_odds_history, 0 as max_odds_history, 20 as min_picks_aarzb, -10000 as min_aarzb, -8 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 3 as stake, false as action_pick union all
SELECT 13 AS order_pick, 20 AS min_picks, -10000 as min_odds_history, -8 as max_odds_history, 20 as min_picks_aarzb, -8 as min_aarzb, 0 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 1 as stake, false as action_pick union all
SELECT 14 AS order_pick, 20 AS min_picks, 2.5 as min_odds_history, 12 as max_odds_history, -1 as min_picks_aarzb, -10000 as min_aarzb, 0 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 1 as stake, true as action_pick union all
SELECT 15 AS order_pick, 20 AS min_picks, 2.5 as min_odds_history, 12 as max_odds_history, 20 as min_picks_aarzb, -10000 as min_aarzb, 0 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 1 as stake, true as action_pick union all
SELECT 16 AS order_pick, 20 AS min_picks, 0 as min_odds_history, 2.5 as max_odds_history, -1 as min_picks_aarzb, -10000 as min_aarzb, 0 as max_aarzb, 0 as min_odds_active, 10000 as max_odds_active, 1 as stake, false as action_pick union all
SELECT 17 AS order_pick, -1 AS min_picks, 0 as min_odds_history, 10000 as max_odds_history, -1 as min_picks_aarzb, 0 as min_aarzb, 10000 as max_aarzb, -10000 as min_odds_active, 10000 as max_odds_active, 1 as stake, false as action_pick
)
insert into pick_order select * from pick_order